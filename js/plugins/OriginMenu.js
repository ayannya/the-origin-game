Scene_Menu.prototype.createBackground = function() {
    this._backgroundSprite = new TilingSprite();
    this._backgroundSprite.move(0,0,Graphics.width,Graphics.height);
    this._backgroundSprite.bitmap = ImageManager.loadSystem("windowbg");
    this.addChild(this._backgroundSprite);
}

Scene_File.prototype.createBackground = function() {
    this._backgroundSprite = new TilingSprite();
    this._backgroundSprite.move(0,0,Graphics.width,Graphics.height);
    this._backgroundSprite.bitmap = ImageManager.loadSystem("savebg");
    this.addChild(this._backgroundSprite);
}

Scene_Item.prototype.createBackground = function() {
    this._backgroundSprite = new TilingSprite();
    this._backgroundSprite.move(0,0,Graphics.width,Graphics.height);
    this._backgroundSprite.bitmap = ImageManager.loadSystem("briefcasebg");
    this.addChild(this._backgroundSprite);
}

Window_Base.prototype.drawPicture = function(pictureName, x, y) {
    var bitmap = ImageManager.loadPicture(pictureName);
    var pw = bitmap.width;
    var ph = bitmap.height;
    var sx = 0;
    var sy = 0;
    this.contents.blt(bitmap, sx, sy, pw, ph, x, y);
};



    var _Scene_Item_create = Scene_Item.prototype.create;
    Scene_Item.prototype.create = function() {
    	_Scene_Item_create.call(this);
    	this._myWindow = new My_Window(400, 50);
    	this.addChild(this._myWindow);
    	this._myWindow.opacity = 150;
    };

    Scene_Item.prototype.update = function () {
    	Scene_ItemBase.prototype.update.call(this);

    	this._myWindow.contents.clear();
    	var item = this._itemWindow._data[this._itemWindow.index()];
		if (item) {
    		this._myWindow.drawPicture(item.id.toString(), 0, 0);
    		//this._myWindow.drawPicture(this.item().itemId.toString(), 0, 0);
    	}

    	/*if (this.item() && $gameParty.hasItem($dataItems[this.item().itemId])) {
    		this._myWindow.drawText(this._itemWindow.index().toString(), 0, 0);
    		//this._myWindow.drawPicture(this.item().itemId.toString(), 0, 0);
    	}*/
    	
    	
    };



	function My_Window() {
		this.initialize.apply(this, arguments);
	};

	My_Window.prototype = Object.create(Window_Base.prototype);
	My_Window.prototype.constructor = My_Window;

	My_Window.prototype.initialize = function(x,y) {
		Window_Base.prototype.initialize.call(this, x, y, 370, 370);


		this.refresh();
	};

	My_Window.prototype.refresh = function() {
		this.contents.clear();
		//this.drawText(this._itemWindow._index, 0, 0);
		//this.drawText('1', 0, 0);
		//this.drawText(TouchInput.y,0,0);
	};


function Window_mItemList() {
    this.initialize.apply(this, arguments);
}

Window_mItemList.prototype = Object.create(Window_ItemList.prototype);
Window_mItemList.prototype.constructor = Window_mItemList;


Window_mItemList.prototype.maxCols = function() {
    return 2;
};

function Scene_Memo() {
    this.initialize.apply(this, arguments);
}

Scene_Memo.prototype = Object.create(Scene_ItemBase.prototype);
Scene_Memo.prototype.constructor = Scene_Memo;

Scene_Memo.prototype.initialize = function() {
    Scene_ItemBase.prototype.initialize.call(this);
};

Scene_Memo.prototype.create = function() {
    Scene_ItemBase.prototype.create.call(this);
    this.createHelpWindow();
    this.createCategoryWindow();
    this.createItemWindow();
    this.createActorWindow();
    this._helpWindow.y = this._itemWindow.height;
    this._itemWindow.y = 0;
    this._actorWindow.width = 0;
    this._actorWindow.height = 0;
    this._itemWindow.opacity = 0;
};

Scene_Memo.prototype.createCategoryWindow = function() {

};

Scene_Memo.prototype.createItemWindow = function() {
    		var wy = this._helpWindow.height;
		    var wh = Graphics.boxHeight - wy;
		    this._itemWindow = new Window_mItemList(0, wy, Graphics.boxWidth*2, wh);
		    this._itemWindow.setHelpWindow(this._helpWindow);
		    //this._itemWindow.setHandler('ok',     this.onItemOk.bind(this));
		    this._itemWindow.setHandler('cancel', this.popScene.bind(this));
		    this._itemWindow.setCategory('keyItem');

		    this.addWindow(this._itemWindow);
		    this._itemWindow.activate();
		    this._itemWindow.select(0);
};

Scene_Memo.prototype.user = function() {
    var members = $gameParty.movableMembers();
    var bestActor = members[0];
    var bestPha = 0;
    for (var i = 0; i < members.length; i++) {
        if (members[i].pha > bestPha) {
            bestPha = members[i].pha;
            bestActor = members[i];
        }
    }
    return bestActor;
};

Scene_Memo.prototype.onCategoryOk = function() {
    this._itemWindow.activate();
    this._itemWindow.selectLast();
};

Scene_Memo.prototype.onItemOk = function() {
    $gameParty.setLastItem(this.item());
    this.determineItem();
    $gameTemp.reserveCommonEvent(35);
};

Scene_Memo.prototype.onItemCancel = function() {
    this._itemWindow.deselect();
    this._categoryWindow.activate();
};

Scene_Memo.prototype.playSeForItem = function() {
    SoundManager.playUseItem();
};

Scene_Memo.prototype.useItem = function() {
    Scene_ItemBase.prototype.useItem.call(this);
    this._itemWindow.redrawCurrentItem();
};

Scene_Memo.prototype.createBackground = function() {
    this._backgroundSprite = new TilingSprite();
    this._backgroundSprite.move(0,0,Graphics.width,Graphics.height);
    this._backgroundSprite.bitmap = ImageManager.loadSystem("memobg");
    this.addChild(this._backgroundSprite);
}


